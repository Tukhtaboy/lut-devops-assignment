# specify a suitable source image
FROM node:latest

# set working directory
WORKDIR /app

# Clean up existing node_modules and package-lock.json
RUN rm -rf node_modules package-lock.json

# Copy package.json and package-lock.json
COPY package*.json ./



# Install dependencies
RUN npm ci 

# copy the application source code files
COPY . .

# Build the application 
# RUN npm run build

EXPOSE 3000

# specify the command which runs the application
CMD ["node", "index.js"]
